import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {SQLite, SQLiteObject} from '@ionic-native/sqlite';

import 'rxjs/add/operator/map'

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  private db: SQLiteObject;
  private isOpen: boolean;

  constructor(public http: HttpClient, public storage: SQLite) {
    if (!this.isOpen) {
      this.storage = new SQLite();
      this.storage.create({name: 'data.bd', location: 'default'}).then((db: SQLiteObject) => {
        this.db = db;
        db.executeSql('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT,' +
          'IDENTIFICATION INTEGER, name TEXT, lastname TEXT)', []);
        this.isOpen = true;
      }).catch((error) => {
        console.log(error);
      });
    }
  }

  createUser(identification: string, name: string, lastname:string) {
    return new Promise(((resolve, reject) => {
      let sql = 'INSERT INTO users (identification, name, lastname) VALUES (?,?,?)';
      this.db.executeSql(sql,[identification, name, lastname]).then((data)=> {
        resolve(data);
      }, (error) =>{
        reject(error);
      });
    }));
  }

  getAllUsers() {
    return new Promise(((resolve, reject) => {
      this.db.executeSql('SELECT * FROM users',[]).then((data) => {
        let arrayUsers: any = [];
        if( data.rows.length> 0) {
          for (var i = 0; i< data.rows.length; i++) {
            arrayUsers.push({
              id: data.rows.item(i).id,
              identification: data.rows.item(i).identification,
              name: data.rows.item(i).name,
              lastname: data.rows.item(i).lastname,
            });
          }
        }
        resolve(arrayUsers);
      }, (error) => {
        reject(error);
      });
    }));
  }

  deleteAllUser(){
    return new Promise((resolve, reject) => {
      let sql = "DELETE FROM users";
      this.db.executeSql(sql,[]).then((data)=> {
        resolve(data);
      },(error)=> {
        reject(error);
      });
    });
  }

  queryUsers(name: string) {
    let arrayUsers: any = [];
    return new Promise(((resolve, reject) => {
      let sql = "SELECT * FROM users WHERE name LIKE '%"+ name + "%'";
      this.db.executeSql(sql, []).then((data)=> {
        if( data.rows.length> 0) {
          for (var i = 0; i< data.rows.length; i++) {
            arrayUsers.push({
              id: data.rows.item(i).id,
              identification: data.rows.item(i).identification,
              name: data.rows.item(i).name,
              lastname: data.rows.item(i).lastname,
            });
          }
        }
        resolve(arrayUsers);
      }, (error) => {
        reject(error);
      });
    }));

  }

}

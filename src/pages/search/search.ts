import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DatabaseProvider} from "../../providers/database/database";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  searchQuery: string = '';
  items: string[];


  constructor(public navCtrl: NavController, public navParams: NavParams, private database: DatabaseProvider) {
    this.initializeItems();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  initializeItems() {
    this.items = [];
    // this.database.getAllUsers().then((data) => {
    //   for (let i =0 ; i<5000; i++){
    //     this.items.push(data[i].name);
    //   }
    //   // console.log(data);
    // }, (error) => {
    //   console.log(error);
    // });


  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    this.database.queryUsers(val).then((data)=>{
     console.log(data[0].name);
    },(error) =>{
      console.log(error);
    });
    // if (val && val.trim() != '') {
    //   console.log(val);
    //   this.items = this.items.filter((item) => {
    //     return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }
  }

}

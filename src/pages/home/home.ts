import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import {AddDataPage} from '../add-data/add-data';
import {EditDataPage} from '../edit-data/edit-data';
import {DatabaseProvider} from "../../providers/database/database";
import {SearchPage} from "../search/search";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  expenses: any = [];
  totalIncome = 0;
  totalExpense = 0;
  balance = 0;

  constructor(public navCtrl: NavController,
              private sqlite: SQLite,
              private database: DatabaseProvider) {

  }

  createUser() {

    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    // for (var i = 0; i < 5; i++){
    //   text += possible.charAt(Math.floor(Math.random() * possible.length));
    // }


    // this.database.deleteAllUser().then((data) => {
    //   console.log(data);
    // }, (error) => {
    //   console.log(error);
    // });

    for (let i = 0; i < 5000; i++) {
      for (let j = 0; j < 5; j++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      this.database.createUser('1', text, 'zaman').then((data) => {
        console.log(data);
      }, (error) => {
        console.log(error);
      });

      text = '';
    }

  }

  getAllUsers() {
    this.database.getAllUsers().then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error);
    });
  }

  ionViewDidLoad() {
    // this.getData();
  }

  ionViewWillEnter() {
    console.log('working on it');
    // this.getData();
  }

  getData() {
    this.sqlite.deleteDatabase({name: 'ionicdb.db', location: 'default'}).then((db: SQLiteObject) => {
      console.log('testing');
    });

    // this.sqlite.create({
    //   name: 'ionicdb.db',
    //   location: 'default'
    // }).then((db: SQLiteObject) => {
    //   db.executeSql('DROP TABLE IF EXISTS danceMoves', )
    //     .then(() => console.log('Executed SQL'))
    //     .catch(e => console.log('Error man !!!!'));
    // }).catch(e => console.log(e));
  }

  addData() {
    this.navCtrl.push(AddDataPage);
  }

  editData(rowid) {
    this.navCtrl.push(EditDataPage, {
      rowid: rowid
    });
  }

  deleteData(rowid) {
    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('DELETE FROM expense WHERE rowid=?', [rowid])
        .then(res => {
          console.log(res);
          this.getData();
        })
        .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  searchTapped() {
    this.navCtrl.push(SearchPage);
  }

}
